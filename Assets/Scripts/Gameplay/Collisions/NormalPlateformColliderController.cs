using UnityEngine;
using Gameplay.Actors;

namespace Gameplay.Collisions
{
    /// <summary>
    /// Class for controlling the comportement of a normal plateform element object and the player during their collision.
    /// </summary>
    public class NormalPlateformColliderController : BaseColliderController
    {
        /// <summary>
        /// Player which collides the plateform.
        /// </summary>
        private PlayerController activePlayer;

        void OnCollisionEnter2D(Collision2D collision)
        {
            // The action is placed to FixedUpdate to handle the shield when it disappears.
            activePlayer = collision.gameObject.GetComponent<PlayerController>();
        }

        void FixedUpdate()
        {
            if (activePlayer)
            {
                var player = activePlayer;

                // this.bounds return the full tilemap collider, not the bounds of the "collided" tiles.
                // var willHurtPlayer = player.Bounds.center.y <= this.Bounds.max.y;
                // Solution, using this custom method : GetCollidedTile(...)
                // Getting the corner of the player collider object
                // We are only interested by the bottom right, and top right corners,
                // for the collision with decor on the x axis.
                // Can be simplified with only used "min" and "max" instead of "center"
                Vector3 rightBottomCorner = player.Bounds.center; 
                rightBottomCorner.x += player.Bounds.extents.x;
                rightBottomCorner.y -= player.Bounds.extents.y;

                Vector3 rightTopCorner = player.Bounds.max;


                // Checking if the player left collided a plateform

                // Why offset :
                //     The collided tile coord is the current position of the player
                //     but the collide happen before the players move, so
                //     we need to add an offset x for getting the next tile on the right (top and bottom) */
                if (GetCollidedTile(rightBottomCorner, new Vector3(0.05f, +0.4f, 0)) != null
                    || GetCollidedTile(rightTopCorner, new Vector3(0.05f, -0.4f, 0)) != null)
                {
                    if (!player.HasShield())
                    {
                        Debug.Log("Player died due to left collided");
                        level.OnDie();
                    }
                }
            }
        }

        void OnCollisionExit2D()
        {
            activePlayer = null;
        }
    }
}