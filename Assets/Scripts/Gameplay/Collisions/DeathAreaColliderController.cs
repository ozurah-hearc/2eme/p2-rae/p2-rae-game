using UnityEngine;
using Gameplay.Actors;

namespace Gameplay.Collisions
{
    /// <summary>
    /// Class for controlling the comportement of the death area object and the player during their collision.
    /// </summary>
    public class DeathAreaColliderController : BaseColliderController
    {
        void OnTriggerEnter2D(Collider2D collision)
        {
            var player = collision.gameObject.GetComponent<PlayerController>();
            if (player != null && collision is BoxCollider2D) //The collider must be the "player hitbox" and not the "magnet hitbox"
            {
                Debug.Log("Player was died due falling to the void");
                level.OnDie();
            }
        }
    }
}