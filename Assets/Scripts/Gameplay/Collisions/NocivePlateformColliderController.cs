using UnityEngine;
using Gameplay.Actors;

namespace Gameplay.Collisions
{

    /// <summary>
    /// Class for controlling the comportement of a nocive plateform element object and the player during their collision.
    /// </summary>
    public class NocivePlateformColliderController : BaseColliderController
    {
        /// <summary>
        /// Player which collides the plateform.
        /// </summary>
        private PlayerController activePlayer;

        void OnCollisionEnter2D(Collision2D collision)
        {
            // The action is placed to FixedUpdate, for handling the shield when it disappears

            activePlayer = collision.gameObject.GetComponent<PlayerController>();
        }

        void FixedUpdate()
        {
            if (activePlayer != null)
            {
                if (!activePlayer.HasShield())
                {
                    Debug.Log("Player died on nocive");
                    level.OnDie();
                    activePlayer = null; //The player is dead, we can lose his reference
                }
            }
        }

        void OnCollisionExit2D()
        {
            activePlayer = null;
        }
    }
}