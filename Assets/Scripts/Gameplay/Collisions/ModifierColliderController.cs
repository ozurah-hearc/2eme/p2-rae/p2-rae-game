using UnityEngine;
using Framework.Data;
using Gameplay.Actors;

namespace Gameplay.Collisions
{
    /// <summary>
    /// Class for controlling the comportement of a modifier object and the player during their collision.
    /// </summary>
    public class ModifierColliderController : BaseColliderController
    {
        /// <summary>
        /// Type of the collided modifier
        /// </summary>
        [field: SerializeField]
        public ModifierEffectData.ModifierType Type { get; set; } = ModifierEffectData.ModifierType.Unknown;

        void OnTriggerEnter2D(Collider2D collision)
        {
            var player = collision.gameObject.GetComponent<PlayerController>();
            if (player == null || !(collision is BoxCollider2D)) //The collider must be the "player hitbox" and not the "magnet hitbox"
            {
                return;
            }

            gameObject.SetActive(false);

            level.RemoveModifierEffect();
            level.OnHitModifier(new ModifierEffectData(Type, 250));

            switch (Type)
            {
                case ModifierEffectData.ModifierType.Shield:
                    OnShieldCollected(player);
                    return;

                case ModifierEffectData.ModifierType.Magnet:
                    OnMagnetCollected(player);
                    return;

                case ModifierEffectData.ModifierType.Mirror:
                    OnMirrorCollected(player);
                    return;

                case ModifierEffectData.ModifierType.SpeedBuff:
                    OnSpeedBuffCollected(player);
                    return;

                case ModifierEffectData.ModifierType.SpeedDebuff:
                    OnSpeedDebuffCollected(player);
                    return;

            }
        }

        /// <summary>
        /// Action when the collided modifier is of type <see cref="ModifierType.Shield"/>.
        /// </summary>
        /// <param name="player">Player who collided the modifier</param>
        private void OnShieldCollected(PlayerController player)
        {
            Debug.Log("Shield collected!");
        }

        /// <summary>
        /// Action when the collided modifier is of type <see cref="ModifierType.Magnet"/>.
        /// </summary>
        /// <param name="player">Player who collided the modifier</param>
        private void OnMagnetCollected(PlayerController player)
        {
            Debug.Log("Magnet collected!");
        }

        /// <summary>
        /// Action when the collided modifier is of type <see cref="ModifierType.Mirror"/>.
        /// </summary>
        /// <param name="player">Player who collided the modifier</param>
        private void OnMirrorCollected(PlayerController player)
        {
            Debug.Log("Mirror collected!");
        }

        /// <summary>
        /// Action when the collided modifier is of type <see cref="ModifierType.SpeedBuff"/>.
        /// </summary>
        /// <param name="player">Player who collided the modifier</param>
        private void OnSpeedBuffCollected(PlayerController player)
        {
            Debug.Log("Speed buff collected!");
        }

        /// <summary>
        /// Action when the collided modifier is of type <see cref="ModifierType.SpeedDebuff"/>.
        /// </summary>
        /// <param name="player">Player who collided the modifier</param>
        private void OnSpeedDebuffCollected(PlayerController player)
        {
            Debug.Log("Speed debuff collected!");
        }
    }
}