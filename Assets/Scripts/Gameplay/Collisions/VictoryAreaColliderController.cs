using UnityEngine;
using Gameplay.Actors;

namespace Gameplay.Collisions
{

    /// <summary>
    /// Class for controlling the comportement of the death area object and the player during their collision.
    /// </summary>
    public class VictoryAreaColliderController : BaseColliderController
    {
        void OnTriggerEnter2D(Collider2D collision)
        {
            var player = collision.gameObject.GetComponent<PlayerController>();
            if (player != null && collision is BoxCollider2D)
            {
                level.OnFinish();
                Debug.Log("Player finished the level");
            }
        }
    }
}