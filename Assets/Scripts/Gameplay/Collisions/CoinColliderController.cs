using UnityEngine;
using Gameplay.Actors;

namespace Gameplay.Collisions
{
    /// <summary>
    /// Class for controlling the comportement of a coin object and the player during their collision.
    /// </summary>
    public class CoinColliderController : BaseColliderController
    {
        /// <summary>
        /// Audio which will be played when a coin is collected.
        /// </summary>
        [field: SerializeField]
        public AudioClip ClipOnCollect { get; set; } = null;

        /// <summary>
        /// Where the owner of this class will go.
        /// </summary>
        private GameObject target = null;

        /// <summary>
        /// Speed at which the object moves against the <see cref="target"/>.
        /// </summary>
        private const float MOVE_SPEED = 8.0f;

        /// <summary>
        /// Initiale position of the object
        /// </summary>
        /// <remarks>If the object is moved (like with the magnet modifier), use this for replace the object.</remarks>
        private Vector3 startPosition;

        new void Awake()
        { // Override of the awake of the base class
            base.Awake();
            startPosition = transform.position;
        }

        void FixedUpdate()
        {
            if (target == null)
            {
                return;
            }

            // Move to the target
            float step = MOVE_SPEED * Time.fixedDeltaTime;
            transform.position = Vector3.MoveTowards(transform.position, target.transform.position, step);
        }

        void OnTriggerEnter2D(Collider2D collision)
        {
            var player = collision.gameObject.GetComponent<PlayerController>();
            if (player == null)
            {
                return;
            }

            // Collision with the hitbox when magnet modifier
            if (collision is CircleCollider2D)
            {
                target = player.gameObject;
            }
            else
            {
                Debug.Log("Player collect coin");
                level.PlayGlobalSound(ClipOnCollect, 1.0f);
                gameObject.SetActive(false);
                target = null;
                level.OnHitCoin();
            }
        }

        /// <summary>
        /// Reset the position of owner of this class to it's initial position.
        /// </summary>
        public void ResetCoinPosition()
        {
            transform.position = startPosition;
        }
    }
}