using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using Gameplay.Levels;
using Framework.Utils;

namespace Gameplay.Collisions
{
    /// <summary>
    /// Base class for controlling the comportement a collision occurs between two objects.
    /// </summary>
    public class BaseColliderController : MonoBehaviour
    {
        /// <summary>
        /// Reference on the level component
        /// </summary>
        /// <remarks>This reference is getting through the componenent of the player <see cref="GameObject.Find(_scripts)"/></remarks>
        protected Level level;

        /// <summary>
        /// Reference on linked tilemap to the script
        /// </summary>
        protected Tilemap linkedTilemap;

        /// <summary>
        /// Reference on the collider which triggered the collider controller.
        /// </summary>
        protected Collider2D collider2d;

        /// <summary>
        /// Bounding box which collided the collider.
        /// </summary>
        public Bounds Bounds
        {
            get { return collider2d.bounds; }
        }


        protected void Awake()
        {
            linkedTilemap = gameObject.GetComponent<Tilemap>();
            collider2d = GetComponent<Collider2D>();
            level = GameObject.Find("_scripts").GetComponent<Level>();
        }

        /// <summary>
        /// Get the tile at the position of the <paramref name="coord"/> (+ the <paramref name="offset"/>).
        /// </summary>
        /// <param name="coord">The "base" coordinate</param>
        /// <param name="offset">Offset of the tile to get (ie, (1,0,0) to get the next tile right of the player).</param>
        /// <returns>The tile who was collided</returns>
        /// <remarks>
        /// Sources : https://answers.unity.com/questions/1572343/how-to-detect-on-which-exact-tile-a-collision-happ.html
        /// https://www.youtube.com/watch?v=7QmgaO2YqG4
        /// </remarks>
        protected TileBase GetCollidedTile(Vector3 coord, Vector3 offset = new Vector3())
        {
            Vector3Int collidedTileCoord = linkedTilemap.WorldToCell(coord + offset); //Must be a vector3Int for the "GetTile(...)"


            return linkedTilemap.GetTile(collidedTileCoord);
        }

        #region old/unused code (when objects was placed into tilemap instead of GameObject)
        /// <summary>
        /// Remove a tile asset from the <see cref="linkedTilemap"/>.
        /// </summary>
        /// <param name="coord">Coordinate of the tile to remove</param>
        /// <param name="offset">Offset of the coordinate</param>
        protected void RemoveTileAt(Vector3 coord, Vector3 offset = new Vector3())
        {
            Vector3Int tileCoordToRemove = linkedTilemap.WorldToCell(coord + offset);

            linkedTilemap.SetTile(tileCoordToRemove, null);
        }

        /// <summary>
        /// DEBUG METHOD !! (The tilemap doesn't contains a method to get each tiles of it)
        /// 
        /// Get the coord of each tiles of the tilemap (only availibe for 2D tilemap).
        /// </summary>
        /// <param name="xStart">
        /// The start index of the x axes tiles to get.
        /// This value must be lower or equals of <paramref name="xEnd"/>.
        /// </param>
        /// <param name="xEnd"> The end index of the x axes tiles to get   </param>
        /// <param name="yStart">
        /// The start index of the y axes tiles to get.
        /// This value must be lower or equals of <paramref name="yEnd"/>.
        /// </param>
        /// <param name="yEnd"> The end index of the x axes tiles to get.</param>
        /// <param name="zAxe"> The z axis to get the tiles.</param>
        /// <returns>Coord of each tiles within the limits.</returns>
        protected List<Point> GetTilesCoordOfTilemap(int xStart, int xEnd, int yStart, int yEnd, int zAxe = 0)
        {
            if (xStart > xEnd)
                throw new System.Exception("xStart is higher than the xEnd !");
            if (yStart > yEnd)
                throw new System.Exception("yStart is higher than the yEnd !");


            var result = new List<Point>();

            for (int x = xStart; x < xEnd; x++)
            {
                for (int y = yStart; y < yEnd; y++)
                {
                    var hasTile = linkedTilemap.HasTile(new Vector3Int(x, y, zAxe));
                    if (hasTile)
                    {
                        result.Add(new Point(x, y));

                    }
                }
            }

            return result;
        }
        #endregion old/unused code
    }
}