using System.Collections;
using UnityEngine;
using Framework.Data;
using Gameplay.Levels;

namespace Gameplay.Actors
{
    /// <summary>
    /// This class is used by the player game object to control it (like physics)
    /// </summary>
    public class PlayerController : MonoBehaviour
    {
        /// <summary>
        /// The horizontal speed in tile per second
        /// </summary>
        [field: SerializeField]
        public float SpeedX { get; set; } = 2.5f;
        /// <summary>
        /// The jump speed just after jumping in tile per second
        /// </summary>
        [field: SerializeField]
        public float JumpBoost { get; set; } = 7.0f;
        /// <summary>
        /// The vertical speed in tile per second^2
        /// </summary>
        [field: SerializeField]
        public float Gravity { get; set; } = 12.0f;
        /// <summary>
        /// The audio clip played when the player jumps
        /// </summary>
        [field: SerializeField]
        public AudioClip ClipOnJump { get; set; } = null;
        /// <summary>
        /// The audio clip played when the player dies
        /// </summary>
        [field: SerializeField]
        public AudioClip ClipOnDeath { get; set; } = null;
        /// <summary>
        /// The audio clip played when the played lands on ground
        /// </summary>
        [field: SerializeField]
        public AudioClip ClipOnLandOnGround { get; set; } = null;
        /// <summary>
        /// One of the two audio clip played when the player walks
        /// </summary>
        [field: SerializeField]
        public AudioClip ClipOnWalk1 { get; set; } = null;
        /// <summary>
        /// One of the two audio clip played when the player walks
        /// </summary>
        [field: SerializeField]
        public AudioClip ClipOnWalk2 { get; set; } = null;

        
        /// <summary>
        /// The collider used to detect walls, coins, modifiers, triggers, etc.
        /// </summary>
        private BoxCollider2D collider2d;
        /// <summary>
        /// The collider used by the magnet to magnetize coins near the player
        /// </summary>
        private CircleCollider2D coinCollider2d;
        
        /// <summary>
        /// The rectangle that represents the collision
        /// </summary>
        public Bounds Bounds
        {
            get { return collider2d.bounds; }
        }

        /// <summary>
        /// The rigid body used for physics (collisions)
        /// </summary>
        private Rigidbody2D body;
        
        public Vector2 velocity;
        /// <summary>
        /// The vector that represents the velocity of the different axis
        /// </summary>
        public Vector2 Velocity
        {
            get { return velocity; }
            set { velocity = value; }
        }

        /// <summary>
        /// The camera game object that exists in the editor
        /// </summary>
        private Camera playerCamera;

        /// <summary>
        /// Does the player wants to jump
        /// </summary>
        public bool WantsToJump { get; set; } = false;
        
        /// <summary>
        /// The active modifier effect
        /// </summary>
        private ModifierEffectData activeEffect = null;
        
        /// <summary>
        /// The current position of the character
        /// </summary>
        public Vector3 Position
        {
            get { return body.position; }
            set { body.position = value; }
        }

        /// <summary>
        /// The animator game object that exists in the editor
        /// </summary>
        private Animator animator;

        /// <summary>
        /// Is the player currently dying
        /// </summary>
        public bool IsDying { get; set; } = false;

        /// <summary>
        /// Is the level finished
        /// </summary>
        public bool IsLevelFinished { get; private set; } = false;
        
        /// <summary>
        /// The audio source used to plays the audio clips related to the player
        /// </summary>
        private AudioSource audioSrc;

        /// <summary>
        /// The next audio clip to play when the player walks (alternate between two clips)
        /// </summary>
        private AudioClip nextWalkClip;
        
        /// <summary>
        /// Counter used to detect when the next walk clip is played
        /// </summary>
        private float walkCounter = 0.0f;

        /// <summary>
        /// Boolean that allows to know if the player just landed on the ground (which plays the land clip)
        /// </summary>
        private bool wasOnGroundLastFrame;

        public delegate void DieFinished();
        /// <summary>
        /// This event is raised when the death animation ends
        /// </summary>
        public event DieFinished OnDieFinished;

        // Start is called before the first frame update
        void Awake()
        {
            collider2d = GetComponent<BoxCollider2D>();
            coinCollider2d = GetComponent<CircleCollider2D>();
            coinCollider2d.enabled = false;
            body = GetComponent<Rigidbody2D>();
            playerCamera = gameObject.transform.GetChild(0).GetComponent<Camera>();
            animator = GetComponent<Animator>();
            audioSrc = GetComponent<AudioSource>();
            nextWalkClip = ClipOnWalk1;
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown("space") || Input.GetKeyDown("up") || Input.GetMouseButtonDown(0))
            {
                WantsToJump = true;
            }

            if (Input.GetKeyDown(KeyCode.F))
            {
                body.position += new Vector2(0.0f, 0.01f);
            }
        }

        /// <summary>
        /// Returns true if the player is on the ground
        /// </summary>
        bool isOnGround()
        {
            float halfWidth = collider2d.size.x / 2.0f;
            float rayDistance = collider2d.size.y / 2.0f + 0.05f;

            // For each raycast (one on the left, one in the middle, one on the right)
            for (int i = -1; i <= 1; i++)
            {
                Vector2 from = new Vector2(body.position.x + i * (halfWidth), body.position.y);
                RaycastHit2D[] hits = Physics2D.RaycastAll(from, Vector2.down, rayDistance);

                foreach (RaycastHit2D hit in hits)
                {
                    if (hit.collider.name == "Terrain" ||
                        (hit.collider.name == "Nocive" && HasShield()))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Removes the current modifier effect (does nothing if the player doesn't have any modifiers)
        /// </summary>
        public void removeModifierEffect()
        {
            if (activeEffect == null)
            {
                return;
            }

            if (activeEffect.Type == ModifierEffectData.ModifierType.Mirror)
            {
                playerCamera.orthographicSize *= -1.0f;
            }
            else if (activeEffect.Type == ModifierEffectData.ModifierType.Magnet)
            {
                coinCollider2d.enabled = false;
            }

            activeEffect = null;
        }

        /// <summary>
        /// Sets the player modifier effect, only called when the player doesn't have an active modifier
        /// </summary>
        public void SetModifierEffect(ModifierEffectData modifierEffect)
        {
            activeEffect = modifierEffect;

            if (modifierEffect.Type == ModifierEffectData.ModifierType.Mirror)
            {
                playerCamera.orthographicSize *= -1.0f;
                return;
            }

            if (modifierEffect.Type == ModifierEffectData.ModifierType.Magnet)
            {
                coinCollider2d.enabled = true;
                return;
            }
        }

        
        /// <summary>
        /// Return true if the player currently has a shield
        /// </summary>
        public bool HasShield()
        {
            if (activeEffect == null)
            {
                return false;
            }

            return activeEffect.Type == ModifierEffectData.ModifierType.Shield;
        }

        /// <summary>
        /// Gets a value that is multiplied to the speed, ex: 2.0 means that the player sould go two times as fast
        /// </summary>
        private float GetSpeedMultiplier()
        {
            if (activeEffect == null)
            {
                return 1.0f;
            }

            if (activeEffect.Type == ModifierEffectData.ModifierType.SpeedBuff)
            {
                return 2.0f;
            }

            if (activeEffect.Type == ModifierEffectData.ModifierType.SpeedDebuff)
            {
                return 0.5f;
            }

            return 1.0f;
        }

        void FixedUpdate()
        {
            if (IsDying)
            {
                return;
            }

            if (!IsLevelFinished)
                RunUpdate();
            else
                EndLevelUpdate();
        }

        /// <summary>
        /// Function called every frame as long as the player should move
        /// </summary>
        private void RunUpdate()
        {
            velocity.x = SpeedX * GetSpeedMultiplier();
            if (!isOnGround())
                velocity.y -= Gravity * Time.fixedDeltaTime;

            body.MovePosition(body.position + velocity * Time.fixedDeltaTime);

            if (isOnGround())
            {
                if (!wasOnGroundLastFrame)
                {
                    wasOnGroundLastFrame = true;
                    audioSrc.PlayOneShot(ClipOnLandOnGround, 1.0f);
                }

                walkCounter += Time.fixedDeltaTime;

                if (walkCounter >= 0.15f)
                {
                    audioSrc.PlayOneShot(nextWalkClip, 0.25f);
                    ToggleNextWalkClip();
                    walkCounter = 0.0f;
                }

                if (WantsToJump)
                {
                    audioSrc.PlayOneShot(ClipOnJump, 1.0f);
                    velocity.y = JumpBoost;
                }
                else if (velocity.y < 0.0f)
                {
                    velocity.y = 0.0f;
                }
            }
            else
            {
                wasOnGroundLastFrame = false;
                walkCounter = 0.0f;
            }

            WantsToJump = false;
        }

        /// <summary>
        /// Toggle the value of nextWalkClip between ClipOnWalk1 and ClipOnWalk2
        /// </summary>
        private void ToggleNextWalkClip()
        {
            if (nextWalkClip == ClipOnWalk1)
            {
                nextWalkClip = ClipOnWalk2;
            }
            else
            {
                nextWalkClip = ClipOnWalk1;
            }
        }
        
        /// <summary>
        /// Called when the player finishes the level
        /// </summary>
        private void EndLevelUpdate()
        {
            velocity.x = 0;
            velocity.y -= Gravity * Time.fixedDeltaTime;

            if (isOnGround())
            {
                velocity.y = JumpBoost;
            }

            body.MovePosition(body.position + velocity * Time.fixedDeltaTime);

        }

        /// <summary>
        /// Called by the level script when it's the end of a level
        /// </summary>
        public void OnLevelFinished()
        {
            IsLevelFinished = true;
            animator.SetBool("Victory", true);
        }

        /// <summary>
        /// Called by the level script when the player dies
        /// </summary>
        public void OnDie()
        {
            if (!IsDying)
            {
                IsDying = true;
                animator.SetBool("Dying", true); //Dying is a variable set in the animator
                audioSrc.PlayOneShot(ClipOnDeath, 1.0f);
                StartCoroutine(DieCoroutine());
            }
        }

        /// <summary>
        /// Async wait the end of the animation
        /// </summary>
        IEnumerator DieCoroutine()
        {
            // Source: https://docs.unity3d.com/ScriptReference/WaitForSeconds.html
            yield return new WaitForSeconds(Level.DURATION_SEC_DIE_ANIMATION);

            if (IsDying) //Dying wasn't canceled
            {
                animator.SetBool("Dying", false);
                IsDying = false;

                OnDieFinished?.Invoke();
            }
        }

        /// <summary>
        /// Resets the player animation
        /// </summary>
        public void RestartAnimation()
        {
            animator.SetBool("Dying", false);
            animator.SetBool("Victory", false);
        }
    }
}
