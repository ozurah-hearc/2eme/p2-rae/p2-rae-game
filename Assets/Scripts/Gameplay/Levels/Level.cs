using System.Collections;
using UI;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using Framework.Data;
using Framework.Managers;
using Gameplay.Collisions;
using Gameplay.Actors;

namespace Gameplay.Levels
{

    /// <summary>
    /// This class is the core of the played level, it retransmit the data to the player and perform action which happens to the level.
    /// </summary>
    public class Level : MonoBehaviour
    {
        /// <summary>
        /// The duration in seconds of the victory animation.
        /// After this delay, the level is completly finished and the next step can be done.
        /// </summary>
        public const float DURATION_SEC_VICTORY_ANIMATION = 3;
        /// <summary>
        /// The duration in seconds of the death animation.
        /// After this delay, the player is completly die and the next step can be done.
        /// </summary>
        public const float DURATION_SEC_DIE_ANIMATION = 1;

        /// <summary>
        /// Music that will be played during the level.
        /// </summary>
        [field: SerializeField]
        public AudioClip Music { get; set; } = null;

        #region GameObject + Component
        /// <summary>
        /// Reference to the player component
        /// </summary>
        /// <remarks>This reference is getting thourgh the componenent of the player <see cref="GameObject.Find(Player)"/>.</remarks>
        private PlayerController player;
        /// <summary>
        /// Reference to the pause menu component
        /// </summary>
        /// <remarks>This reference is getting thourgh the componenent of the player <see cref="GameObject.Find(PauseMenuCanvas)"/>.</remarks>
        private PauseMenu pauseMenu;
        /// <summary>
        /// Reference to the first spawn (start of the level) of the player
        /// </summary>
        /// <remarks>This reference is getting thourgh the componenent of the player <see cref="GameObject.Find(SpawnArea)"/>.</remarks>
        private GameObject spawnArea;
        /// <summary>
        /// Reference to the victory area (end of the level)
        /// </summary>
        /// <remarks>This reference is getting thourgh the componenent of the player <see cref="GameObject.Find(VictoryArea)"/>.</remarks>
        private GameObject victoryArea;

        #region HUD
        /// <summary>
        /// Reference to the text "number of collected coins" component
        /// </summary>
        /// <remarks>This reference is getting thourgh the componenent of the player <see cref="GameObject.Find(TxtCoinCollected)"/>.</remarks>
        private TextMeshProUGUI txtCollectedCoin;
        /// <summary>
        /// Reference to the text "attempt #" component
        /// </summary>
        /// <remarks>This reference is getting thourgh the componenent of the player <see cref="GameObject.Find(TxtAttempt)"/>.</remarks>
        private TextMeshProUGUI txtAttempt;
        /// <summary>
        /// Reference to the text "modifier duration before finish" component
        /// </summary>
        /// <remarks>This reference is getting thourgh the componenent of the player <see cref="GameObject.Find(TxtModifierDuration)"/>.</remarks>
        private TextMeshProUGUI txtModifierDuration;
        /// <summary>
        /// Reference to the HUD modifier part, when a modifier is active
        /// </summary>
        /// <remarks>This reference is getting thourgh the componenent of the player <see cref="GameObject.Find(ModifierHUD)"/>.</remarks>
        private GameObject hudModifier;
        /// <summary>
        /// Reference to the active modifier sprite
        /// </summary>
        /// <remarks>This reference is getting thourgh the componenent of the player <see cref="GameObject.Find(SpriteActiveModifier)"/>.</remarks>
        private SpriteRenderer currentModifierSprite;
        /// <summary>
        /// Reference to the background of the HUD "modifier + coins", when a modifier is active
        /// </summary>
        /// <remarks>This reference is getting thourgh the componenent of the player <see cref="GameObject.Find(BgHUD)"/>.</remarks>
        private GameObject bgHudPanel;
        /// <summary>
        /// Reference to the background of the HUD "modifier + coins", when no modifier is active
        /// </summary>
        /// <remarks>This reference is getting thourgh the componenent of the player <see cref="GameObject.Find(BgHUDNoModifier)"/>.</remarks>
        private GameObject bgHudNoModifierPanel;
        /// <summary>
        /// Reference to line of the progression of the level
        /// </summary>
        /// <remarks>This reference is getting thourgh the componenent of the player <see cref="GameObject.Find(ProgressBarBottom)"/>.</remarks>
        private GameObject progressBarBottom;
        /// <summary>
        /// Reference to the "cursor" of the progression of the level
        /// </summary>
        /// <remarks>This reference is getting thourgh the componenent of the player <see cref="GameObject.Find(DinoProgressBar)"/>.</remarks>
        private GameObject dinoProgression;
        #endregion HUD
        #endregion GameObject + Component

        /// <summary>
        /// The active modifier
        /// </summary>
        /// <remarks>Can be null if there is no active modifier.</remarks>
        private ModifierEffectData CurrentModifier { get; set; }

        /// <summary>
        /// The spawn position of the player in this level
        /// </summary>
        /// <remarks>Setted in init according the <see cref="spawnArea"/>.</remarks>
        private Vector3 spawnPosition;

        /// <summary>
        /// Number of die (attempt) on this run attempt.
        /// </summary>
        private int QtyDie { get; set; } = 0;
        /// <summary>
        /// Number of collected coins on this run attempt.
        /// </summary>
        private int CollectedCoins { get; set; } = 0;

        /// <summary>
        /// Field for the property <see cref="CurrentDistance"/>.
        /// </summary>
        /// <remarks>Considere using <see cref="CurrentDistance"/> instead of this field.</remarks>
        private float currentDistance = 1.0f;
        /// <summary>
        /// Current distance (unit from the <see cref="spawnPosition"/> axe X) of this run attempt.
        /// Additional set action : set the current distance (units and percents) acordidng the distance of the run
        /// + set <see cref="MaxDistance"/> and <see cref="MaxDistancePercent"/> if new record reached.
        /// </summary>
        public float CurrentDistance
        {
            get { return currentDistance; }
            set
            {
                currentDistance = value;
                CurrentDistancePercent = currentDistance / MaxLevelDistance;

                if (MaxDistance < currentDistance)
                {
                    MaxDistance = currentDistance;
                    MaxDistancePercent = currentDistance / MaxLevelDistance;
                }
            }
        }

        /// <summary>
        /// Field for the property <see cref="CurrentDistancePercent"/>.
        /// </summary>
        /// <remarks>Considere using <see cref="CurrentDistancePercent"/> instead of this field.</remarks>
        private float currentDistancePercent = 0.0f;
        /// <summary>
        /// Percentage of the current distance (from <see cref="spawnArea"/> to <see cref="victoryArea"/> position (only X-axis)).
        /// </summary>
        /// <remarks>The value is from 0 (0%) to 1 (100%).</remarks>
        public float CurrentDistancePercent
        {
            get { return currentDistancePercent; }
            set
            {
                currentDistancePercent = Mathf.Clamp(value, 0, 1);
            }
        }

        /// <summary>
        /// Maximal distance (unit from the <see cref="spawnArea"/> to <see cref="victoryArea"/> position (only X-axis)) of this level.
        /// </summary>
        private float MaxLevelDistance { get; set; } = 0.0f;
        /// <summary>
        /// Maximal distance reached by the player (unit from the <see cref="spawnPosition"/> X-axis) of this level.
        /// </summary>
        private float MaxDistance { get; set; } = 0.0f;
        /// <summary>
        /// Field for the property <see cref="MaxDistancePercent"/>
        /// </summary>
        /// <remarks>Considere using <see cref="MaxDistancePercent"/> instead of this field.</remarks>
        private float maxDistancePercent = 0.0f;

        /// <summary>
        /// Percentage of the maximal distance reached by the player (from <see cref="spawnArea"/> to <see cref="victoryArea"/> position (only X-axis)) of this level.
        /// </summary>
        /// <remarks>The value is from 0 (0%) to 1 (100%).</remarks>
        private float MaxDistancePercent
        {
            get { return maxDistancePercent; }
            set
            {
                maxDistancePercent = Mathf.Clamp(Mathf.Ceil(value * 100f) / 100f, 0, 1);
            }
        }

        /// <summary>
        /// Name of the level (the scene name)
        /// </summary>
        private string LevelName;

        /// <summary>
        /// The audio source who will play <see cref="Music"/> in loop.
        /// </summary>
        private AudioSource audioSourceMusic;
        /// <summary>
        /// The audio source that will play sounds when external elements (like coins) emit a sound.
        /// </summary>
        private AudioSource audioSourceClips;

        /// <summary>
        /// Initialize the objects of the level + update HUD.
        /// </summary>
        /// <remarks>The reference of GameObject should have been done before callign this method.</remarks>
        public void Init()
        {
            LevelName = SceneManager.GetActiveScene().name;

            // Events
            player.OnDieFinished += OnAfterDie;
            pauseMenu.OnRestartEvent += OnRestart;
            pauseMenu.OnQuitEvent += GoToMainMenu;
            pauseMenu.OnPauseStateChange += OnPauseStateChange;

            // Spawn
            spawnPosition = spawnArea.transform.position;

            player.Position = spawnPosition;

            // Distance
            MaxLevelDistance = victoryArea.transform.position.x - spawnArea.transform.position.x + player.Bounds.max.x; //player.Bounds.max for getting the right part of the player collider

            // Set the skin
            player.GetComponent<Animator>().runtimeAnimatorController = SkinManager.Instance.GetAnimatorCharacter(SaveManager.Instance.GameDatas.CurrentSkin);
            dinoProgression.GetComponent<SpriteRenderer>().sprite = SkinManager.Instance.GetSpriteProgression(SaveManager.Instance.GameDatas.CurrentSkin);

            // Updating the HUD to the default value
            UpdateHudCoins();
            UpdateHudAttempt();
            UpdateHudProgression();
        }

        public void Start()
        {
            // Start is called before the first frame update

            // Use start, to be sure the player awake is firstly done
            // https://gamedevbeginner.com/start-vs-awake-in-unity/
            // The player GameObject should have already been initialized with his script

            // Get the objects of the level
            spawnArea = GameObject.Find("SpawnArea");
            victoryArea = GameObject.Find("VictoryArea");
            player = GameObject.Find("Player").GetComponent<PlayerController>();
            pauseMenu = GameObject.Find("PauseMenuCanvas").GetComponent<PauseMenu>();

            // HUD object/component
            txtCollectedCoin = GameObject.Find("TxtCoinCollected").GetComponent<TextMeshProUGUI>();
            hudModifier = GameObject.Find("ModifierHUD");
            bgHudPanel = GameObject.Find("BgHUD");
            bgHudNoModifierPanel = GameObject.Find("BgHUDNoModifier");
            currentModifierSprite = GameObject.Find("SpriteActiveModifier").GetComponent<SpriteRenderer>();
            txtModifierDuration = GameObject.Find("TxtModifierDuration").GetComponent<TextMeshProUGUI>();
            txtAttempt = GameObject.Find("TxtAttempt").GetComponent<TextMeshProUGUI>();

            progressBarBottom = GameObject.Find("ProgressBarBottom");
            dinoProgression = GameObject.Find("DinoProgressBar");

            // Audio
            audioSourceMusic = gameObject.AddComponent<AudioSource>() as AudioSource;
            audioSourceMusic.clip = Music;
            audioSourceMusic.playOnAwake = false;
            audioSourceMusic.loop = true;
            audioSourceMusic.volume = 0.2f;
            audioSourceMusic.Play();

            audioSourceClips = gameObject.AddComponent<AudioSource>() as AudioSource;
            audioSourceClips.playOnAwake = false;


            // Init the level to the beginning
            Init();

            // Additional task
            int totalCoins = GameObject.Find("Coins").transform.childCount;
            Debug.Log("[LEVEL / COINS] found " + totalCoins + " coins on this level");
            LevelManager.Instance.UpdateDatas(LevelName, new LevelData(totalCoins));
        }

        public void FixedUpdate()
        {
            // Progression
            CurrentDistance = player.Position.x - spawnPosition.x;
            UpdateHudProgression();
            // Debug.Log(string.Format("Current distance = {0:F2} / Records = {1:F2} ({2:F2}%)/ max = {3:F2}", config.CurrentDistance, config.MaxDistance, config.MaxDistancePercent * 100, config.MaxlevelDistance));

            if (CurrentModifier != null)
            {
                // HUD with modifier
                hudModifier.SetActive(true);
                bgHudNoModifierPanel.SetActive(false);
                bgHudPanel.SetActive(true);

                txtModifierDuration.text = string.Format("{0:N2}", CurrentModifier.Duration);

                // Modifier behavior
                CurrentModifier.Duration -= Time.fixedDeltaTime;

                if (CurrentModifier.Duration <= 0)
                {
                    RemoveModifierEffect();
                }
            }
            else
            {
                // HUD without modifier
                hudModifier.SetActive(false);
                bgHudNoModifierPanel.SetActive(true);
                bgHudPanel.SetActive(false);
            }

        }

        /// <summary>
        /// Play a sound a single time.
        /// </summary>
        /// <param name="clip">Audio to play</param>
        /// <param name="volumeScale">Volume intensity of the audio (0 to 1)</param>
        public void PlayGlobalSound(AudioClip clip, float volumeScale = 1.0f)
        {
            Debug.Log(clip);
            Debug.Log("PLAYING CLIP");
            audioSourceClips.PlayOneShot(clip, volumeScale);
        }

        /// <summary>
        /// Action triggered when the player die.
        /// </summary>
        public void OnDie()
        {
            if (!player.IsDying) //For avoiding multi triggering of this action
            {
                QtyDie++;

                player.OnDie(); //Start the animation of die + respawn
                Debug.Log(string.Format("Number of attempts = {0}", QtyDie));
                LevelManager.Instance.UpdateDatas(LevelName, new LevelData(CollectedCoins, MaxDistancePercent));
            }
        }

        /// <summary>
        /// Action triggered when the player reaches the end of the level.
        /// </summary>
        public void OnFinish()
        {
            player.OnLevelFinished();

            pauseMenu.CanPaused = false;

            StartCoroutine(OnFinishCoroutine()); //Async wait 

            // Store the config info (like collected coins, die, ...) in the GameData class
            LevelManager.Instance.UpdateDatas(LevelName, new LevelData(CollectedCoins, MaxDistancePercent));
        }

        /// <summary>
        /// Async wait some times before calling the next step of the finished level action
        /// </summary>
        /// <returns></returns>
        IEnumerator OnFinishCoroutine()
        {
            // https://docs.unity3d.com/ScriptReference/WaitForSeconds.html
            // Wait for the end of the animation
            yield return new WaitForSeconds(DURATION_SEC_VICTORY_ANIMATION);

            SceneManager.LoadScene("MainMenu");
        }

        /// <summary>
        /// Load the scene "Main Menu".
        /// </summary>
        private void GoToMainMenu()
        {
            SceneManager.LoadScene("MainMenu");
        }

        /// <summary>
        /// Action triggered when a coin is collected.
        /// </summary>
        public void OnHitCoin()
        {
            CollectedCoins++;

            UpdateHudCoins();
        }

        /// <summary>
        /// Update the coins HUD content.
        /// </summary>
        private void UpdateHudCoins()
        {
            Debug.Log(string.Format("Number of coins collected = {0}", CollectedCoins));

            txtCollectedCoin.text = CollectedCoins.ToString();
        }

        /// <summary>
        /// Update the attempt HUD content.
        /// </summary>
        private void UpdateHudAttempt()
        {
            txtAttempt.text = string.Format("Attempt {0}", QtyDie + 1);
        }

        /// <summary>
        /// Update the progression HUD content.
        /// </summary>
        private void UpdateHudProgression()
        {
            // Size of the progress bar
            var progressBarSize = progressBarBottom.transform.localScale.x;

            // Calc offset of the dino Progress Bar
            float leftOffset = progressBarSize * 2 * CurrentDistancePercent - progressBarSize; //Progress bar is from -scale to +scale (= scale*2)

            // Apply the offset
            // Source for change offset : https://answers.unity.com/questions/888257/access-left-right-top-and-bottom-of-recttransform.html
            RectTransform dinoRT = dinoProgression.GetComponent<RectTransform>();
            dinoRT.offsetMin = new Vector2(leftOffset, dinoRT.offsetMin.y); // new Vector2(left, bottom)
        }

        /// <summary>
        /// Remove the active modifier and its effects.
        /// </summary>
        public void RemoveModifierEffect()
        {
            CurrentModifier = null;
            player.removeModifierEffect();
        }

        /// <summary>
        /// Action triggered when a modifier is recolted.
        /// </summary>
        /// <param name="modifierEffect">Datas of the modifier</param>
        public void OnHitModifier(ModifierEffectData modifierEffect)
        {
            CurrentModifier = modifierEffect;
            currentModifierSprite.sprite = SkinManager.Instance.GetModifierSprite(CurrentModifier.Type);

            player.SetModifierEffect(modifierEffect);
        }

        /// <summary>
        /// Action triggered after the die animation ended.
        /// </summary>
        public void OnAfterDie()
        {
            // Remove the modifier
            RemoveModifierEffect();

            // Reset the pieces into the scene
            GameObject coins = GameObject.Find("Coins");
            for (int i = 0; i < coins.transform.childCount; i++)
            {
                coins.transform.GetChild(i).gameObject.GetComponent<CoinColliderController>().ResetCoinPosition();
                coins.transform.GetChild(i).gameObject.SetActive(true);
            }

            // Reset the modifier into the scene
            GameObject modifiers = GameObject.Find("Modifiers");
            for (int i = 0; i < modifiers.transform.childCount; i++)
                modifiers.transform.GetChild(i).gameObject.SetActive(true);

            // Reset the collected pieces of the player
            CollectedCoins = 0;

            // Updating the HUD
            UpdateHudCoins();
            UpdateHudAttempt();

            // Respawn the player and reset velocity
            player.WantsToJump = false;
            player.Position = spawnPosition;
            player.Velocity = new Vector2(0.0f, 0.0f);

            // Reset positions of the sky parallax objects
            GameObject.Find("Sky").GetComponent<ParallaxBackground>().ResetPos();
            GameObject.Find("Cloud1").GetComponent<ParallaxBackground>().ResetPos();
            GameObject.Find("Cloud2").GetComponent<ParallaxBackground>().ResetPos();
            GameObject.Find("CloudsGroup").GetComponent<ParallaxBackground>().ResetPos();
        }

        /// <summary>
        /// Action triggered when the pause menu is opened or closed.
        /// </summary>
        private void OnPauseStateChange(bool isPaused)
        {
            if (isPaused)
            {
                audioSourceMusic.Pause();
            }
            else
            {
                audioSourceMusic.Play();
                player.WantsToJump = false;
            }
        }

        /// <summary>
        /// Action triggered when restarting the level.
        /// </summary>
        public void OnRestart()
        {
            // Moving the character to the spawn
            player.Position = spawnPosition;

            // Reseting level (it's like a die, but not a die (that's why IsDying = false)
            OnAfterDie();
            player.IsDying = false;

            // Cancelling animations of the player (like die)
            player.RestartAnimation();

            // Reseting variables
            CollectedCoins = 0;
            QtyDie = 0;
            MaxDistance = 0;
            MaxDistancePercent = 0;

            // Updating the HUD
            UpdateHudCoins();
            UpdateHudAttempt();
        }
    }
}