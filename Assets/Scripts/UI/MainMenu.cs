using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Framework.Managers;

namespace UI
{
    /// <summary>
    /// Main menu management class.
    /// </summary>
    public class MainMenu : MonoBehaviour
    {
        private Button PreviousLevelButton;
        private Button NextLevelButton;
        private Button StartLevelButton;
        private Button AboutButton;
        private Button ExitButton;
        private Button ShopButton;
        private Image LevelPreviewImage;
        private GameObject ProgressBackgroundLeftPart;
        private GameObject ProgressBackgroundMiddlePart;
        private GameObject ProgressBackgroundRightPart;

        private GameObject ProgressLeftPart;
        private GameObject ProgressMiddlePart;
        private GameObject ProgressRightPart;

        private GameObject AboutPanel;
        private Button AboutPanelButton;
        private Button AboutInnerPanelButton;

        private int SelectedLevelIndex = 0;

        private void Awake()
        {
            PreviousLevelButton = GameObject.Find("PreviousLevelButton").GetComponent<Button>();
            NextLevelButton = GameObject.Find("NextLevelButton").GetComponent<Button>();
            StartLevelButton = GameObject.Find("StartLevelButton").GetComponent<Button>();
            ExitButton = GameObject.Find("ExitButton").GetComponent<Button>();
            ShopButton = GameObject.Find("ShopButton").GetComponent<Button>();
            LevelPreviewImage = GameObject.Find("LevelPreview").GetComponent<Image>();
            AboutButton = GameObject.Find("AboutButton").GetComponent<Button>();
            AboutPanel = GameObject.Find("AboutPanel");
            AboutPanel.SetActive(false);
            AboutPanelButton = AboutPanel.GetComponent<Button>();
            AboutInnerPanelButton = AboutPanel.transform.Find("AboutInnerPanel").GetComponent<Button>();

            GameObject progressBarBackground = GameObject.Find("ProgressBarBackground");
            ProgressBackgroundLeftPart = progressBarBackground.transform.Find("Left").gameObject;
            ProgressBackgroundMiddlePart = progressBarBackground.transform.Find("Middle").gameObject;
            ProgressBackgroundRightPart = progressBarBackground.transform.Find("Right").gameObject;


            GameObject progressBar = GameObject.Find("ProgressBarFill");
            ProgressLeftPart = progressBar.transform.Find("Left").gameObject;
            ProgressMiddlePart = progressBar.transform.Find("Middle").gameObject;
            ProgressRightPart = progressBar.transform.Find("Right").gameObject;
        }

        void OnEnable()
        {
            PreviousLevelButton.onClick.AddListener(() => SelectPreviousLevel());
            NextLevelButton.onClick.AddListener(() => SelectNextLevel());
            StartLevelButton.onClick.AddListener(() => StartSelectedLevel());
            ExitButton.onClick.AddListener(() => ExitGame());
            AboutButton.onClick.AddListener(() => ToggleAboutPanel());
            ShopButton.onClick.AddListener(() => LoadShop());
            AboutPanelButton.onClick.AddListener(() => ToggleAboutPanel());
            AboutInnerPanelButton.onClick.AddListener(() => { /* no op */ });

            UpdateLevelPreview();
        }

        void OnDisable()
        {
            PreviousLevelButton.onClick.RemoveAllListeners();
            NextLevelButton.onClick.RemoveAllListeners();
            StartLevelButton.onClick.RemoveAllListeners();
            ExitButton.onClick.RemoveAllListeners();
            AboutButton.onClick.RemoveAllListeners();
            ShopButton.onClick.RemoveAllListeners();
            AboutPanelButton.onClick.RemoveAllListeners();
            AboutInnerPanelButton.onClick.RemoveAllListeners();
        }

        /// <summary>
        /// Update the level preview according to the selected <see cref="Level"/>.
        /// </summary>
        private void UpdateLevelPreview()
        {
            Debug.Log("[MAIN MENU] currently selected preview index : " + SelectedLevelIndex);

            // Level preview
            LevelPreviewImage.sprite = Resources.Load<Sprite>("GUI/Backgrounds/Levels/Level" + (SelectedLevelIndex + 1));

            // Progress bar
            UpdateProgressBar();
        }

        /// <summary>
        /// Update the selected bar according to the selected <see cref="Level"/>.
        /// </summary>
        private void UpdateProgressBar()
        {
            float currentMaxDistancePercentage = (float) SaveManager.Instance.GameDatas.Levels["Level" + (SelectedLevelIndex + 1)].MaxDistance;

            float leftWidthFull = ProgressBackgroundLeftPart.GetComponent<RectTransform>().sizeDelta.x;
            float middleWidthFull = ProgressBackgroundMiddlePart.GetComponent<RectTransform>().sizeDelta.x;
            float rightWidthFull = ProgressBackgroundRightPart.GetComponent<RectTransform>().sizeDelta.x;

            float totalWidth = leftWidthFull + middleWidthFull + rightWidthFull;

            Debug.Log("[MAIN MENU / PROGRESS BAR] progress percentage " + currentMaxDistancePercentage);
            Debug.Log("[MAIN MENU / PROGRESS BAR] total length " + totalWidth);

            float progressWidth = currentMaxDistancePercentage * totalWidth;

            // left part
            if (progressWidth == 0)
            {
                ProgressLeftPart.SetActive(false);
            }
            else
            {
                ProgressLeftPart.SetActive(true);
            }

            Debug.Log("[MAIN MENU / PROGRESS BAR] left part " + (ProgressLeftPart.activeSelf ? "ON" : "OFF"));

            // middle part
            float minTriggerPercentage = leftWidthFull / totalWidth;
            float maxTriggerPercentage = (totalWidth - leftWidthFull) / totalWidth;

            Debug.Log("[MAIN MENU / PROGRESS BAR] <" + minTriggerPercentage + " ; " + maxTriggerPercentage + ">");
            if (currentMaxDistancePercentage >= minTriggerPercentage)
            {
                ProgressMiddlePart.SetActive(true);
                float middleWidth = currentMaxDistancePercentage * middleWidthFull;
                ProgressMiddlePart.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, middleWidth);
            }
            else
            {
                ProgressMiddlePart.SetActive(false);
            }

            Debug.Log("[MAIN MENU / PROGRESS BAR] middle part " + (ProgressMiddlePart.activeSelf ? "ON" : "OFF"));

            // right part
            if (progressWidth == totalWidth)
            {
                ProgressRightPart.SetActive(true);
            }
            else
            {
                ProgressRightPart.SetActive(false);
            }

            Debug.Log("[MAIN MENU / PROGRESS BAR] right part " + (ProgressRightPart.activeSelf ? "ON" : "OFF"));
        }

        /// <summary>
        /// Select next available level.
        /// </summary>
        /// <remarks>If last level is already selected, go back to the first one.</remarks>
        private void SelectNextLevel()
        {
            Debug.Log("[MAIN MENU] select next preview");
            SelectedLevelIndex = (SelectedLevelIndex + 1) % SaveManager.Instance.GameDatas.Levels.Count;
            UpdateLevelPreview();
        }

        /// <summary>
        /// Select previous available level.
        /// </summary>
        /// <remarks>If first level is already selected, go to the last one.</remarks>
        private void SelectPreviousLevel()
        {
            Debug.Log("[MAIN MENU] select previous preview");
            int levelCount = SaveManager.Instance.GameDatas.Levels.Count;
            SelectedLevelIndex = (SelectedLevelIndex + levelCount - 1) % levelCount;
            UpdateLevelPreview();
        }

        /// <summary>
        /// Run the selected level.
        /// </summary>
        private void StartSelectedLevel()
        {
            SceneManager.LoadScene("Level" + (SelectedLevelIndex + 1));
        }

        /// <summary>
        /// Toggle the about panel.
        /// </summary>
        private void ToggleAboutPanel()
        {
            AboutPanel.SetActive(!AboutPanel.activeSelf);
        }

        private void LoadShop()
        {
            SceneManager.LoadScene("Shop");
        }

        /// <summary>
        /// Exit the game.
        /// </summary>
        private void ExitGame()
        {
            Application.Quit(0);
        }
    }
}