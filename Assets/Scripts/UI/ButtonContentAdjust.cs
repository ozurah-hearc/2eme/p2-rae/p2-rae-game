using UnityEngine;

namespace UI
{
    /// <summary>
    /// Adjust content in a button when clicking on it.
    /// </summary>
    public class ButtonContentAdjust : MonoBehaviour
    {

        /// <summary>
        /// Movement value, can be adapted in the editor.
        /// </summary>
        public float MovementValue = 4;
        private Vector3 AdjustVector
        {
            get
            {
                return new Vector3(0, MovementValue, 0);
            }
        }

        /// <summary>
        /// Triggered when we need to adjust.
        /// </summary>
        public void AdjusteOn()
        {
            transform.localPosition -= AdjustVector;
        }

        /// <summary>
        /// Triggered when we cancel the adjustment.
        /// </summary>
        public void AdjustOff()
        {
            transform.localPosition += AdjustVector;
        }
    }
}