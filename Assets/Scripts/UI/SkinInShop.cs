using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Framework.Managers;

/// <summary>
/// This class purpose is to manage the skins that are available in the shop.
/// </summary>
namespace Framework.UI
{
    public class SkinInShop : MonoBehaviour
    {
        /// <summary>
        /// The sprite of the skin.
        /// </summary>
        public Image skinImage;
        
        /// <summary>
        /// The price of the skin.
        /// </summary>
        public int skinPrice;
        
        /// <summary>
        /// The skin's button's text.
        /// </summary>
        public TextMeshProUGUI buttonText;

        /// <summary>
        /// Boolean value to determine if the skin is unlocked or not.
        /// </summary>
        private bool isSkinUnlocked_;
        
        /// <summary>
        /// The type of the skin.
        /// </summary>
        public SkinManager.SkinType skinType;
        
        /// <summary>
        /// The text that contains the total of coins we can spend to the shop
        /// </summary>
        public TextMeshProUGUI totalCoins;

        /// <summary>
        /// Is called when the script instance is being loaded. Determine if the skin is unlocked and instanciate the texts.
        /// </summary>
        private void Awake()
        {
            buttonText.text = skinPrice.ToString();

            if (SaveManager.Instance.GameDatas.UnlockedSkins.Contains(skinType))
            {
                buttonText.text = "Equip";
                isSkinUnlocked_ = true;
            }

            if (skinType == SaveManager.Instance.GameDatas.CurrentSkin)
            {
                buttonText.text = "Equiped";
            }

            totalCoins.text = GetTotalCoinsToSpend().ToString();
        }

        /// <summary>
        /// Is called every frame. Check if the skin is no longer selected.
        /// </summary>
        private void Update()
        {
            if (SaveManager.Instance.GameDatas.CurrentSkin != skinType && SaveManager.Instance.GameDatas.UnlockedSkins.Contains(skinType))
            {
                buttonText.text = "Equip";
            }
        }

        /// <summary>
        /// Is called when we pressed on a skin's button. Equip the skin or buy it, regarding the money we have and the skins that are unlocked.
        /// </summary>
        public void OnButtonPressed()
        {
            if (isSkinUnlocked_)
            {
                Equip();
                SaveManager.Instance.SaveGameDatas();
                return;
            }

            // Try to buy
            if (GetTotalCoinsToSpend() >= skinPrice)
            {
                SaveManager.Instance.GameDatas.UnlockedSkins.Add(skinType);
                SaveManager.Instance.GameDatas.SpentCoins = SaveManager.Instance.GameDatas.SpentCoins + skinPrice;
                SaveManager.Instance.SaveGameDatas();
                totalCoins.text = GetTotalCoinsToSpend().ToString();

                Equip();
            }
        }

        /// <summary>
        /// Get the total of coins we can spend on the shop. The value is all the collected coins minus le coins we spent.
        /// </summary>
        /// <returns></returns>
        public int GetTotalCoinsToSpend()
        {
            int collectedCoins = 0;
            try
            {
                foreach (var level in SaveManager.Instance.GameDatas.Levels)
                {
                    collectedCoins += level.Value.CollectedCoins;
                }
                return collectedCoins - SaveManager.Instance.GameDatas.SpentCoins;
            }
            catch
            {
                return 0;
            }

        }

        /// <summary>
        /// Equip a skin.
        /// </summary>
        private void Equip()
        {
            SaveManager.Instance.GameDatas.CurrentSkin = skinType;
            buttonText.text = "Equiped";
            isSkinUnlocked_ = true;
        }
    }
}