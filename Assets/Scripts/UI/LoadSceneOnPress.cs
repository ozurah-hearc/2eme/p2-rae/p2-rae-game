using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI
{
    /// <summary>
    /// Load given scene on button press.
    /// </summary>
    public class LoadSceneOnPress : MonoBehaviour
    {
        public string Scene;

        public void Start()
        {

        }

        void Update()
        {

        }

        public void OnButtonPressed()
        {
            SceneManager.LoadScene(Scene);
        }
    }
}