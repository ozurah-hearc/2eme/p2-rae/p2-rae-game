using UnityEngine;

namespace UI { 
    /// <summary>
    /// This class purpose is to manage the parallax effect.
    /// </summary>
    public class ParallaxBackground : MonoBehaviour
    {

        /// <summary>
        /// The position of the object when it first start the level.
        /// </summary>
        private Vector3 startLevelPos;
    
        /// <summary>
        /// The length of the sprite.
        /// </summary>
        public float length;
    
        /// <summary>
        /// The start position of the sprite.
        /// </summary>
        private float startpos;
    
        /// <summary>
        /// The main camera
        /// </summary>
        public GameObject camera;
    
        /// <summary>
        /// The desired intensity of the parallax effect.
        /// </summary>
        public float parallaxEffect;
    
        /// <summary>
        /// A temporary variable who stores the maximum position the sprite can go before "repeating" himself.
        /// </summary>
        private float temp;
    
        /// <summary>
        /// The distance the sprite has travelled.
        /// </summary>
        private float distance;


        /// <summary>
        /// Is called when the script is enabled. It instantiates the variables needed : startpos, startLevelpos and length.
        /// </summary>
        void Start()
        {
            startLevelPos = transform.position;
            startpos = startLevelPos.x;
            length = GetComponent<SpriteRenderer>().bounds.size.x; // Longueur du sprite
        }

        /// <summary>
        /// Is called every frame. It recalculates the position of the sprite depending of startpost, the distance and temp,
        /// </summary>
        void Update()
        {
            temp = (camera.transform.position.x * (1 - parallaxEffect));

            distance = (camera.transform.position.x * parallaxEffect);

            transform.position = new Vector3(startpos + distance, transform.position.y, transform.position.z);

            if (temp > startpos + length)
            {
                startpos += length;
            }
        }

        /// <summary>
        /// Reset the position of the sprite.
        /// </summary>
        public void ResetPos() 
        {
            startpos = startLevelPos.x;
            temp = 0;
            distance = 0;

            transform.position = new Vector3(startpos, transform.position.y, transform.position.z);
        }
    }
}