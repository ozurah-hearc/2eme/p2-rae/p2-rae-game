﻿namespace Framework.Utils
{
    /// <summary>
    /// Structure representing a 2D point.
    /// </summary>
    public struct Point
    {
        /// <summary>
        /// X-axis coordinate of the point.
        /// </summary>
        public int x;
        /// <summary>
        /// Y-axis coordinate of the point.
        /// </summary>
        public int y;
        public Point(int px, int py)
        {
            x = px;
            y = py;
        }
    }
}