using UnityEngine;
using UnityEngine.SceneManagement;

namespace Framework.Utils
{
    /// <summary>
    /// Load main menu. Mainly used to load the main menu after preload.
    /// </summary>
    public class LoadMainMenu : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {
            SceneManager.LoadScene("MainMenu");
        }

        void Update()
        {

        }
    }
}