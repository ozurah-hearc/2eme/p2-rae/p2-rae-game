using UnityEngine;

namespace Framework.Utils
{
    /// <summary>
    /// Ensure that the current game object will remains until manually destroyed.
    /// Used to keep alive long-term objects like managers.
    /// </summary>
    public class DontDestroyOnLoad : MonoBehaviour
    {
        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
        }
    }
}
