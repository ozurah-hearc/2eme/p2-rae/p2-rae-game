using UnityEngine;

namespace Framework.Utils
{
    /// <summary>
    /// When added to a scene, ensure that the scene contains all preloaded dependencies.
    /// </summary>
    public class EnsurePreloaded : MonoBehaviour
    {
        private void Awake()
        {
            GameObject app = GameObject.Find("_app");
            if (app == null)
            {
                UnityEngine.SceneManagement.SceneManager.LoadScene("_preload");
            }
        }
    }
}