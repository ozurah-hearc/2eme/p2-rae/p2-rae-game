using System;

namespace Framework.Data {
    /// <summary>
    /// This class is used for the persistent level data, such as already collected coins and level progression.
    /// </summary>
    /// <remarks>Serializable</remarks>
    [Serializable]
    public class LevelData
    {
        /// <summary>
        /// The collected coins in the level.
        /// </summary>
        public int CollectedCoins { get; set; }
        /// <summary>
        /// The number of coins in the level.
        /// </summary>
        public int TotalCoins { get; set; }
        /// <summary>
        /// The maximal distance reached by the player in the level.
        /// </summary>
        public double MaxDistance { get; set; }

        public LevelData() : this(0, 0)
        {
        
        }
        public LevelData(int totalCoins) : this(0, totalCoins, 0)
        {
        
        }

        public LevelData(int coins, double maxDistance) : this(coins, 0, maxDistance)
        {

        }

        public LevelData(int coins, int totalCoins, double maxDistance)
        {
            CollectedCoins = coins;
            TotalCoins = totalCoins;
            MaxDistance = maxDistance;
        }
    }
}