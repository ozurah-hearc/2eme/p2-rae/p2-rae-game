namespace Framework.Data {
    /// <summary>
    /// This class is the data of a modifier (contains only data, no modifier behavior).
    /// </summary>
    public class ModifierEffectData
    {
        /// <summary>
        /// Type of the modifier effect
        /// </summary>
        public enum ModifierType
        {
            Unknown,
            Shield,
            Magnet,
            Mirror,
            SpeedBuff,
            SpeedDebuff,
        }

        /// <summary>
        /// Type of the modifier
        /// </summary>
        public ModifierType Type { get; private set; } = ModifierType.Unknown;
        /// <summary>
        /// Improvement level of the modifier
        /// </summary>
        public int Level { get; private set; } = 1;
        /// <summary>
        /// Duration in seconds of the modifier
        /// </summary>
        public double Duration { get; set; } = 4.0;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type">Set <see cref="Type"/></param>
        /// <param name="level">Set <see cref="Level"/</param>
        public ModifierEffectData(ModifierType type, int level)
        {
            Type = type;
            Level = level;
        }
    }
}