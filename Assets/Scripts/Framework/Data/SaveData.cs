﻿using System;
using System.Collections.Generic;
using Framework.Managers;

namespace Framework.Data
{
    /// <summary>
    /// This class is used for the persistent data of the game,
    /// such as level statistics (distance, coins) or the selected skin.
    /// </summary>
    /// <remarks>Serializable</remarks>
    [Serializable]
    public class SaveData
    {
        /// <summary>
        /// Collection of the level data with their statistics, indexed by their name.
        /// </summary>
        public Dictionary<string, LevelData> Levels { get; set; } = new Dictionary<string, LevelData>();

        public int SpentCoins { get; set; } = 0;

        /// <summary>
        /// The selected skin
        /// </summary>
        public SkinManager.SkinType CurrentSkin { get; set; } = SkinManager.SkinType.GreenDino;
        public HashSet<SkinManager.SkinType> UnlockedSkins { get; set; } = new HashSet<SkinManager.SkinType>();
    }
}
