using UnityEngine;
using UnityEngine.SceneManagement;
using Framework.Data;

namespace Framework.Managers
{
    /// <summary>
    /// This class is a manager for the level, it is used for storing the level data when a level is running.
    /// This class must be declared once, and get object with the static property "Instance".
    /// </summary>
    public class LevelManager : MonoBehaviour
    {
        /// <summary>
        /// Get a reference to this class.
        /// </summary>
        public static LevelManager Instance { get; private set; }

        void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Debug.Log("Warning: multiple " + this + " in scene!");
            }
        }
        void Start()
        {
            // Load each scene starting with "Level" and verify if the scene is already present in the GameManager, if not, add the scene
            for (int i = 0; i < SceneManager.sceneCountInBuildSettings; i++)
            {
                string[] scenePathParts = SceneUtility.GetScenePathByBuildIndex(i).Split('/');
                string sceneName = scenePathParts[scenePathParts.Length - 1].Replace(".unity", "");

                if (sceneName.StartsWith("Level"))
                {


                    if (!SaveManager.Instance.GameDatas.Levels.ContainsKey(sceneName))
                    {
                        Debug.Log("[LEVEL MANAGER / LOAD UNSAVED] loaded unsaved level from files : " + sceneName);
                        SaveManager.Instance.GameDatas.Levels.Add(sceneName, new LevelData());
                    }
                }
            }

            // Save the datas (importent if a new scene wasn't saved to the GameManager before
            SaveManager.Instance.SaveGameDatas();
        }


        /// <summary>
        /// Update and save the data to the persistant data file.
        /// </summary>
        /// <param name="levelName">Name of the level</param>
        /// <param name="levelData">Set <see cref="LevelData"/</param>
        public void UpdateDatas(string levelName, LevelData levelData)
        {
            bool changed = false;

            // Create default if do not exist
            if(!SaveManager.Instance.GameDatas.Levels.ContainsKey(levelName))
            {
                SaveManager.Instance.GameDatas.Levels.Add(levelName, new LevelData());
                changed = true;
            }

            // Update data only if higher than before
            LevelData savedLevelData = SaveManager.Instance.GameDatas.Levels[levelName];
            if (levelData.CollectedCoins > savedLevelData.CollectedCoins)
            {
                savedLevelData.CollectedCoins = levelData.CollectedCoins;
                changed = true;
            }
            if(levelData.MaxDistance > savedLevelData.MaxDistance)
            {
                savedLevelData.MaxDistance = levelData.MaxDistance;
                 changed = true;
            }
            if(levelData.TotalCoins > savedLevelData.TotalCoins)
            {
                savedLevelData.TotalCoins = levelData.TotalCoins;
                 changed = true;
            }

            if(changed)
            {
                 SaveManager.Instance.SaveGameDatas();
            }
        }
    }
}