using System.Collections.Generic;
using UnityEngine;
using Framework.Data;

namespace Framework.Managers
{
    /// <summary>
    /// This class is a manager for the skin, it regroups each sprites, animations, etc. per skin.
    /// This class must be declared once, and get object with the static property "Instance".
    /// </summary>
    public class SkinManager : MonoBehaviour
    {
        /// <summary>
        /// Get a reference to this class.
        /// </summary>
        public static SkinManager Instance { get; private set; }

        /// <summary>
        /// Path of character's animations
        /// </summary>
        /// <remarks>The animation must be in the "Assets/Resources/" folder of the project </remarks>
        private const string playerAnimationPath = "Animations/Player/";
        /// <summary>
        /// Path of the character's sprites
        /// </summary>
        /// <remarks>The sprites must be in the "Assets/Resources/" folder of the project </remarks>
        private const string playerSpritePath = "Sprites/Player/";
        /// <summary>
        /// Path of sprites of modifiers
        /// </summary>
        /// <remarks>The sprites must be in the "Assets/Resources/" folder of the project </remarks>
        private const string modifierSpritePath = "Sprites/Modifiers/";

        /// <summary>
        /// Type of availables skins
        /// </summary>
        public enum SkinType
        {
            GreenDino,
            BlueDino,
            RedDino,
            YellowDino,
        }

        /// <summary>
        /// Skin which will be used if the selected skin is missing data (like sprite).
        /// </summary>
        /// <remarks>The default skin should contains each resources</remarks>
        private SkinType defaultSkin;

        /// <summary>
        /// Animation of the skin
        /// </summary>
        private readonly Dictionary<SkinType, RuntimeAnimatorController> characterAnimator = new Dictionary<SkinType, RuntimeAnimatorController>();
        /// <summary>
        /// Sprites of the skins, used by the "progression bar" in a level.
        /// </summary>
        private readonly Dictionary<SkinType, Sprite> progressSprites = new Dictionary<SkinType, Sprite>();
        /// <summary>
        /// Sprites of the skins, used for displaying the skin (like in the shop).
        /// </summary>
        private readonly Dictionary<SkinType, Sprite> defaultSprites = new Dictionary<SkinType, Sprite>();

        /// <summary>
        /// Sprites of the modifiers
        /// </summary>
        private readonly Dictionary<ModifierEffectData.ModifierType, Sprite> modifierSprites = new Dictionary<ModifierEffectData.ModifierType, Sprite>();

        void Awake()
        {
            if (Instance == null)
                Instance = this;
            else
                Debug.Log("Warning: multiple " + this + " in scene!");
        }
        void Start()
        {
            // Default (if element is missing, the skin will get where default points)
            //     that means at least the default points must have ALL resources
            defaultSkin = SkinType.GreenDino;

            // Skin Green dino
            LoadSkin(SkinType.GreenDino, "GreenDino", 1, 19);

            // Skin Blue dino
            LoadSkin(SkinType.BlueDino, "BlueDino", 1, 19);

            // Skin Red dino
            LoadSkin(SkinType.RedDino, "RedDino", 1, 19);

            // Skin Red dino
            LoadSkin(SkinType.YellowDino, "YellowDino", 1, 19);

            // Sprites of the modifiers
            // note : modifiers are in this class, because we can imagine that in future versions,
            //       the modifier icons could be changed like a skin.
            //       Little anticipation for evolution
            LoadModifierSprite(ModifierEffectData.ModifierType.Shield, "shield");
            LoadModifierSprite(ModifierEffectData.ModifierType.Magnet, "magnet");
            LoadModifierSprite(ModifierEffectData.ModifierType.Mirror, "mirror");
            LoadModifierSprite(ModifierEffectData.ModifierType.SpeedBuff, "speed-buff");
            LoadModifierSprite(ModifierEffectData.ModifierType.SpeedDebuff, "speed-debuff");
        }

        /// <summary>
        /// Load the resources (sprites and animations) for the skin.
        /// </summary>
        /// <param name="skin">Skin who will get these resources</param>
        /// <param name="resourceName">Name of the resource (sprites and animation must have the same name)</param>
        /// <param name="defaultSpriteIndex">Index of the default sprite (used for displaying the skin, like the shop)</param>
        /// <param name="progressSpriteIndex">Index of the level progress bar sprite</param>
        private void LoadSkin(SkinType skin, string resourceName, int defaultSpriteIndex, int progressSpriteIndex)
        {
            LoadSprites(skin, resourceName, defaultSpriteIndex, progressSpriteIndex);
            LoadAnimation(skin, resourceName);
        }

        /// <summary>
        /// Load the sprites for the skin.
        /// The sprites should be in a single image, which one is splited into assets with Unity.
        /// </summary>
        /// <param name="skin">Skin who will get these resources</param>
        /// <param name="spriteName">Name of the resource</param>
        /// <param name="defaultSpriteIndex">Index of the default sprite (used for displaying the skin, like the shop)</param>
        /// <param name="progressSpriteIndex">Index of the level progress bar sprite</param>
        private void LoadSprites(SkinType skin, string spriteName, int defaultSpriteIndex, int progressSpriteIndex)
        {
            //This sprites are into a single image; require to load all sprite of this image, and then get the one which we wants        
            defaultSprites.Add(skin, Resources.LoadAll<Sprite>(playerSpritePath + spriteName)[defaultSpriteIndex]);
            progressSprites.Add(skin, Resources.LoadAll<Sprite>(playerSpritePath + spriteName)[progressSpriteIndex]);
        }

        /// <summary>
        /// Load the sprites for the specified modifier.
        /// </summary>
        /// <param name="modifier">Type of the modifier</param>
        /// <param name="spriteName">Sprite used by the modifier</param>
        private void LoadModifierSprite(ModifierEffectData.ModifierType modifier, string spriteName)
        {
            modifierSprites.Add(modifier, Resources.Load<Sprite>(modifierSpritePath + spriteName));
        }


        /// <summary>
        /// Load the animations for the skin.
        /// </summary>
        /// <param name="skin">Skin who will get these resources</param>
        /// <param name="animationName">Name of the resource</param>
        private void LoadAnimation(SkinType skin, string animationName)
        {
            characterAnimator.Add(skin, Resources.Load<RuntimeAnimatorController>(playerAnimationPath + animationName));
        }

        /// <summary>
        /// Get the animation of the skin.
        /// </summary>
        /// <param name="skin">Skin to get the resource</param>
        /// <returns>
        /// Animation associated with the skin.
        /// If the animation of the <see cref="SelectedSkin"/> could not be found, return the animation of the <see cref="defaultSkin"/>.
        /// </returns>
        public RuntimeAnimatorController GetAnimatorCharacter(SkinType skin)
        {
            if (!characterAnimator.ContainsKey(skin))
                skin = defaultSkin;

            return characterAnimator[skin];
        }

        /// <summary>
        /// Get the sprite of the "level progression bar" of the skin
        /// </summary>
        /// <param name="skin">Skin to get the resource</param>
        /// <returns>
        /// Sprite associated to the skin
        /// If the sprite of the <see cref="SelectedSkin" /> could not be found, return the animation of the <see cref="defaultSkin"/>.
        /// </returns>
        public Sprite GetSpriteProgression(SkinType skin)
        {
            if (!progressSprites.ContainsKey(skin))
                skin = defaultSkin;

            return progressSprites[skin];
        }

        /// <summary>
        /// Get the sprite of the modifier.
        /// </summary>
        /// <param name="modifier"><see cref="ModifierEffectData.ModifierType" /> to get the resource</param>
        /// <returns>Sprite associated with the modifier</returns>
        public Sprite GetModifierSprite(ModifierEffectData.ModifierType modifier)
        {
            return modifierSprites[modifier];
        }

        /// <summary>
        /// Get the default sprite of the skin.
        /// </summary>
        /// <param name="skin">Skin to get the resource.</param>
        /// <returns>
        /// Sprite associated with the skin.
        /// </returns>
        /// <remarks>By default, that means the "displaying sprite", like for the shop.</remarks>
        public Sprite GetDefaultSprite(SkinType skin)
        {
            return defaultSprites[skin];
        }
    }
}