using UnityEngine;
using System.IO;
using Newtonsoft.Json;
using Framework.Data;

namespace Framework.Managers {
    /// <summary>
    /// This class is used to manage the saving and the loading of the player's progression.
    /// </summary>
    public class SaveManager : MonoBehaviour
    {
        /// <summary>
        /// Unique instance of the current game manager
        /// </summary>
        public static SaveManager Instance { get; private set; }

        /// <summary>
        /// Current game datas.
        /// </summary>
        /// <remarks>Must not be overriden but mutated.</remarks>
        public SaveData GameDatas { get; private set; } = new SaveData();

        void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Debug.Log("Warning: multiple " + this + " in scene!");
            }

            LoadGameDatas();
        }

        /// <summary>
        /// Save game datas to file system.
        /// </summary>
        public void SaveGameDatas()
        {
            GameDatas.UnlockedSkins.Add(SkinManager.SkinType.GreenDino);

            Debug.Log("[GAME MANAGER / WRITE SAVE] saving game datas : ");
            Debug.Log(JsonConvert.SerializeObject(GameDatas, Formatting.Indented));
            string destination = Application.persistentDataPath + "/save.json";
            File.WriteAllText(destination, JsonConvert.SerializeObject(GameDatas, Formatting.Indented));
        }

        /// <summary>
        /// Load saved game datas from file system.
        /// </summary>
        /// <remarks>Create one if it doesn't already exists.</remarks>
        public void LoadGameDatas()
        {
            string destination = Application.persistentDataPath + "/save.json";

            if (!File.Exists(destination))
            {
                Debug.LogWarning("[GAME MANAGER/LOAD SAVE] no save file found, generating one");
                SaveGameDatas();
                return;
            }

            string fileContent = File.ReadAllText(destination);
            GameDatas = JsonConvert.DeserializeObject<SaveData>(fileContent);

            string[] keys = new string[GameDatas.Levels.Count];
            GameDatas.Levels.Keys.CopyTo(keys, 0);
            Debug.Log("[GAME MANAGER/LOAD SAVE] found game datas: ");
            Debug.Log(JsonConvert.SerializeObject(GameDatas, Formatting.Indented));
            Debug.Log("[GAME MANAGER/LOAD SAVE] game datas loaded");
        }
    }
}