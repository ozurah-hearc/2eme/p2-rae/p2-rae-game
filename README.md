# Run To Avoid Extinction

Run To Avoid Extinction is a small game made with Unity Engine during out studies at HE-Arc.

[Access to the wiki](https://gitlab-etu.ing.he-arc.ch/groups/isc/2021-22/niveau-2/2281-1-projet-p2-il-sa/gr5-rae/-/wikis/home)

![Logo](https://www.he-arc.ch/wp-content/themes/he-arc/static/images/logo-he-arc.svg)
